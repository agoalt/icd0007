<?php


class Person
{
    public $id;
    public $firstName;
    public $lastName;
    public $phones = [];

    public function __construct($id, $firstName, $lastName, $phones) {
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->phones = $phones;
    }
}
