<?php
require_once 'Person.php';

const DSN = 'mysql:host=db.mkalmo.xyz;dbname=agoalt';
const USER = 'agoalt';
const PASSWORD = '752F';

function getContacts($id = null) {
    $pdo = new PDO(DSN, USER, PASSWORD,
        [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
    if (!$id) {
        $getContacts = $pdo->prepare(
            'SELECT * FROM contacts
                       LEFT JOIN phones ON contactId = contacts_contactId'
        );
    } else {
        $getContacts = $pdo->prepare(
            'SELECT * FROM contacts 
                        LEFT JOIN phones ON contactId = contacts_contactId 
                        WHERE contactId = :contactId'
        );
        $getContacts->bindValue(':contactId', $id);
    }
    $getContacts->execute();
    $people = [];
    $ids = [];
    foreach ($getContacts as $contact) {
        if (!in_array($contact['contactId'], $ids)) {
            $person = new Person(
                $contact['contactId'],
                $contact['name'],
                $contact['lastName'],
                [
                    $contact['numberId'] => $contact['number']
                ]
            );
            $ids[] = $contact['contactId'];
            $people[] = $person;
        } else {
            foreach ($people as $key => $value) {
                if ($value->id === $contact['contactId']) {
                    $value->phones[$contact['numberId']] = $contact['number'];
                }
            }
        }
    }
    return $people;
}

function insertContact($postData) {
    $pdo = new PDO(DSN, USER, PASSWORD, [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
    $insertName = $pdo->prepare(
        'INSERT INTO contacts (name, lastName) VALUES (:personName, :personLastName)'
    );
    $insertName->bindValue(':personName', $postData['firstName']);
    $insertName->bindValue(':personLastName', $postData['lastName']);
    $insertName->execute();
    $lastId = $pdo->lastInsertId();
    foreach ($postData as $key => $value) {
        if (strpos($key, "phone") === 0) {
            if ($value != '') {
                $insertPhone = $pdo->prepare(
                    'INSERT INTO phones (number, contacts_contactId) VALUES (:phone, :id)'
                );
                $insertPhone->bindValue(':phone', $value);
                $insertPhone->bindValue(':id', $lastId);
                $insertPhone->execute();
            }
        }
    }
}

function oneDimensionArray($personData) {
    $person = [];
    $person['id'] = $personData[0]->id;
    $person['firstName'] = $personData[0]->firstName;
    $person['lastName'] = $personData[0]->lastName;
    $countPhones = 0;
    foreach ($personData[0]->phones as $id => $phone) {
        $countPhones++;
        if (!empty($phone)) {
            $person["phone$countPhones"] = $phone;
        }
        if (!empty($id)) {
            $person["idPhone$countPhones"] = $id;
        }
        if ($countPhones === 3) {
            break;
        }
    }
    return $person;
}

function editContact($postData, $userId) {
    $currentData = oneDimensionArray(getContacts($userId));
    foreach ($currentData as $cdKey => $cdValue) {
        foreach ($postData as $pdKey => $pdValue) {
            if ($cdKey == $pdKey) {
                if ($cdValue != $pdValue) {
                    $currentData[$cdKey] = $pdValue;
                }
            } else {
                $currentData[$pdKey] = $pdValue;
            }
        }
    }
    updateContactData($currentData);
}

function updateContactData($data) {
    $pdo = new PDO(DSN, USER, PASSWORD, [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
    $contactId = $data['id'];
    if ($data['firstName'] !== '' & $data['lastName'] !== '') {
        $updateName = $pdo->prepare(
            'UPDATE contacts 
                        SET contacts.name = :firstName, contacts.lastName = :lastName 
                        WHERE contacts.contactId = :id'
        );
        $updateName->bindValue(':firstName', $data['firstName']);
        $updateName->bindValue(':lastName', $data['lastName']);
        $updateName->bindValue(':id', $data['id']);
        $updateName->execute();
    }
    $countNumbers = 0;
    foreach ($data as $key => $value) {
        $countNumbers++;
        if (key_exists("phone$countNumbers", $data)) {
            if (key_exists("idPhone$countNumbers", $data)) {
                if ($data["phone$countNumbers"] !== '') {
                    $updateNumber = $pdo->prepare(
                        'UPDATE phones SET phones.number = :num 
                                    WHERE numberId = :numId'
                    );
                    $updateNumber->bindValue(':num', $data["phone$countNumbers"]);
                    $updateNumber->bindValue(':numId', $data["idPhone$countNumbers"]);
                    $updateNumber->execute();
                } else {
                    $deleteNumber = $pdo->prepare(
                        'DELETE FROM phones WHERE numberId = :numId'
                    );
                    $deleteNumber->bindValue(':numId', $data["idPhone$countNumbers"]);
                    $deleteNumber->execute();
                }
            } else if ($data["phone$countNumbers"] !== '') {
                print 'insert';
                print $contactId;
                print $data["phone$countNumbers"];
                $insertNumber = $pdo->prepare(
                    'INSERT INTO phones (number, contacts_contactId) VALUES (:num, :contactId)'
                );
                $insertNumber->bindValue(':num', $data["phone$countNumbers"]);
                $insertNumber->bindValue(':contactId', $contactId);
                $insertNumber->execute();
            }
        }
    }
}

function checkForErrors($postData) {
    $errorMessages = [
        'firstName' => 'First name is missing!',
        'lastName' => 'Last name is missing!',
        'firstTooShort' => 'First name is too short! Minimum length: 2',
        'lastTooShort' => 'Last name is too short! Minimum length: 2'
    ];
    $errors = [];
    $errorMessagesToPrint = [];
    foreach ($postData as $key => $value) {
        if (empty($value)) {
            $errors[] = $key;
        }
        if ($key === 'firstName') {
            if (2 > strlen($value) & strlen($value) > 0) {
                $errors[] = 'firstTooShort';
            }
        }
        if ($key === 'lastName') {
            if (2 > strlen($value) & strlen($value) > 0) {
                $errors[] = 'lastTooShort';
            }
        }
    }
    foreach ($errors as $error) {
        if (array_key_exists($error, $errorMessages)) {
            $errorMessagesToPrint += array($error => $errorMessages[$error]);
        }
    }
    if (empty($errorMessagesToPrint)) {
        return null;
    } else {
        return $errorMessagesToPrint;
    }
}

function deleteContact($id) {
    $pdo = new PDO(DSN, USER, PASSWORD,
        [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
    $deleteEntry = $pdo->prepare(
        'DELETE FROM contacts WHERE contactId = :id'
    );
    $deleteEntry->bindValue(':id', $id);
    $deleteEntry->execute();
    $deleteEntry = $pdo->prepare(
        'DELETE FROM phones WHERE contacts_contactId = :id'
    );
    $deleteEntry->bindValue(':id', $id);
    $deleteEntry->execute();
}
