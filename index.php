<?php
require_once 'vendor/tpl.php';
require_once 'functions/functions.php';
$data = [];
if (isset($_GET['command'])) {
    $command = $_GET['command'];

    if ($command === 'show_add_page') {
        $data = [
            'title' => 'Add page',
            'template' => 'add.html'
        ];
    } else if ($command === 'show_database_list_page') {
        $data = [
            'title' => 'Database list',
            'template' => 'list.html',
            'people' => getContacts()
        ];
    } else if ($command === 'delete') {
        deleteContact($_GET['id']);
        header('Location: /index.php?command=show_database_list_page');
    } else if ($command === 'save') {
        $errors = checkForErrors($_POST);
        if ($errors) {
            $data = [
                'title' => 'Add page',
                'template' => 'add.html',
                'errors' => $errors
            ];
        } else {
            insertContact($_POST);
            header('Location: /index.php?command=show_database_list_page');
        }
    } else if ($command === 'edit') {
        $data = [
            'title' => 'Edit contact',
            'template' => 'edit.html',
            'person' => oneDimensionArray(getContacts($_GET['id']))
        ];
    } else if ($command === 'edit_person') {
        $errors = checkForErrors($_POST);
        if ($errors) {
            $data = [
                'title' => 'Edit contact',
                'template' => 'edit.html',
                'person' => oneDimensionArray(getContacts($_GET['id'])),
                'errors' => $errors
            ];
        } else {
            $id = $_GET['id'];
            editContact($_POST, $id);
            header('Location: /index.php?command=show_database_list_page');
        }
    } else {
        $data = [
            'title' => 'Nothing here yet...',
            'template' => 'empty.html'
        ];
    }

} else {
    header('Location: /index.php?command=show_database_list_page');
}

print renderTemplate('templates/main.html', $data);
